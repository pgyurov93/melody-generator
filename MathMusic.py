from MusicGenerator import MusicGenerator, np, pys

class MathMusic(MusicGenerator):
    """
    This class contains some default functions which generate pre-defined sets of notes. There isn't much freedom
    given to the user in respect to the generation of the melodies as the purpose of this class is to let the user
    explore and familirise oneself with the pysynth module and its basic capabilities.

    The melodies generated from the methods pertain to mathematical notions (numbers and functions), such as using
    the digits of pi and e to select notes from the notes dictionary.

    The methods in this class are quite basic and came about simply from curiosity. One will notice that the melodies
    produced are very similar and 'bassy'; this is because only the numbers 0-9 were used to select notes.
    """

    def __create_song(self, mylist, duration, filename):
        """
        Used as a general method to create the .wav files.

        Parameters
        ----------
        mylist : list
            List of numbers used to index the dictionary of music notes.
        duration : int / float
            The duration for all notes
        filename : string
            The name of the .wav file

        Returns
        -------
            None
        """

        selected_notes = []
        for number in mylist:
            selected_notes.append((self.notes[number], duration))
        pys.make_wav(selected_notes, fn = filename)

    def pi_song(self):
        """
        Default function which generates a .wav file according to the first 100 digits of pi, where each digit is used
        to index the dictionary of musical notes.
        Note duration set to 4.

        Returns
        -------
            None
        """
        pi = '31415926535897932384626433832795028841971693993751058209749445923078164062862089986280348253421170679'
        # add space between each element, then split splits the elements and creates a list
        pi = " ".join(pi).split(" ")
        # convert strings to ints
        for i in range(len(pi)):
            pi[i] = int(pi[i])
        self.__create_song(pi, 4, 'pi_song.wav')

    def e_song(self):
        """
        Default function which generates a .wav file according to the first 100 digits of e, where each digit is used
        to index the dictionary of musical notes.
        Note duration set to 4.

        Returns
        -------
            None
        """
        e = '27182818284590452353602874713526624977572470936999595749669676277240766303535475945713821785251664274'
        # add space between each element, then split splits the elements and creates a list
        e = " ".join(e).split(" ")
        # convert strings to ints
        for i in range(len(e)):
            e[i] = int(e[i])
        self.__create_song(e, 4, 'e_song.wav')

    def random_song(self, random_duration_check):
        """
        Default function which generates random integers used to index the dictionary of musical notes.
        The note duration is either random or set to 4.

        Parameters
        ----------
        random_duration_check : bool
            If True each note is given a random duration value ranging from pysynth's cut off value, 0.52, to 10.0.
            If False the note duration is set to the default value of 4.

        Returns
        -------
            None
        """
        random_ints = list(np.random.random_integers(0, 87, 88))
        selected_notes = []
        if random_duration_check == True:
            for number in random_ints:
                random_duration = np.random.uniform(0.52, 10.0)
                selected_notes.append((self.notes[number], random_duration))
            pys.make_wav(selected_notes, fn='random_song.wav')
        else:
            self.__create_song(random_ints, 4, 'random_song.wav')

